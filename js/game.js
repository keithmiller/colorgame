// RequestAnimFrame: a browser API for getting smooth animations
window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame   ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			window.oRequestAnimationFrame      ||
			window.msRequestAnimationFrame     ||
		function( callback ){
			return window.setTimeout(callback, 1000 / 60);
		};
})();

window.cancelRequestAnimFrame = ( function() {
	return  window.cancelAnimationFrame          ||
			window.webkitCancelRequestAnimationFrame ||
			window.mozCancelRequestAnimationFrame    ||
			window.oCancelRequestAnimationFrame      ||
			window.msCancelRequestAnimationFrame     ||
		clearTimeout;
} )();
//do not touch code above

// //initialize canvas and other variables
var canvas = document.getElementById("gameCanvas"); //Store HTML5 canvas tag into js variable
var gameScreen = document.getElementById("gameScreen");
var context = canvas.getContext("2d"); //Create canvas 2d context
var W = window.innerWidth;
var H = window.innerHeight;
var mouse = {}; //initializes mouse variable to store its current position
var cLeft = (W/2) - 480; //the left edge of the canvas
var cRight = (W/2) + 480; //right edge of the canvas
var cTop = 0;
var cBot = 720;

var flagCollision = 0; //flag variable is changed on collision
var flagGameOver = 0; //flag variable is changed on game over
var init; //variable used to initialize animation
catcher = {};//catcher object

canvas.addEventListener("mousemove", trackPosition, true);
//canvas.addEventListener("mousedown", btnClick, true);
//canvas.addEventListener("mousemove", btnHover, true);

//set the canvas height and width to match the browser size
canvas.width = cRight - cLeft;
canvas.height = cBot - cTop;
console.log(canvas.width + " " + canvas.height);

 function paintCanvas(){
     context.fillStyle = "black";
     context.fillRect(0,0,W,H);
 }

////////////////////////////////////////////
//Game play background on canvas
 function gamePlayBg(){
 	paintHeader();
 	paintTop();
 	paintMid();
 	paintBot();
 	paintPlay();
 }
 function paintHeader(){
 	context.fillStyle = "#252525";
 	context.fillRect(0,0,960,65);
 }
 function paintTop(){
 	context.fillStyle = "#34495e";
 	context.fillRect(0,66,960,166);
 }
 function paintMid(){
 	context.fillStyle = "#1abc9c";
 	context.fillRect(0,231,960,166);
 }
 function paintBot(){
 	context.fillStyle = "#f39c12";
 	context.fillRect(0,397,960,166);
 }
function paintPlay(){
	context.fillStyle = "#c3d4d1";
 	context.fillRect(0,563,960,157);
}
///////////////////////////////////////
///////////////////////////////////////
//menu background on canvas
function menuBg(){
	topBlue();
	midGreen();
	botOrange();
}
function topBlue(){
	context.fillStyle = "#34495e";
 	context.fillRect(0,0,960,239);
}
function midGreen(){
	context.fillStyle = "#1abc9c";
 	context.fillRect(0,239,960,36);
}
function botOrange(){
	context.fillStyle = "#f39c12";
 	context.fillRect(0,275,960,445);
}
/////////////////////////////////////
/////////////////////////////////////
//game header on canvas
function drawHeader(s,m,t){
	drawTime(t);
	drawMiss(m);
	drawScore(s);
}

function drawTime(t){
	context.fillStyle = "white";
	context.font = "20px Aldrich, sans-serif";
	context.textBaseline ="middle";
	context.fillText(t, W-96, 10);
}
function drawMiss(m){
	context.fillStyle = "white";
	context.font = "20px Aldrich, sans-serif";
	context.textBaseline ="middle";
	context.fillText(m, 432, 10);
}
function drawScore(s){
	context.fillStyle = "white";
	context.font = "20px Aldrich, sans-serif";
	context.textBaseline ="middle";
	context.fillText(s + "Score", 96, 10);
}


function trackPosition(event){
    mouse.x = event.pageX;
    mouse.y = event.pageY;
    
    if(mouse.y>=(cTop+65) && mouse.y<(cTop+231)){
    	//console.log("blue!");
    	$('#catcher').css( 'background-color', '#34495e' );
    	catcherColor = 3;
    }else if(mouse.y>=(cTop+231) && mouse.y<(cTop+397)){
    	//console.log("green!");
    	$('#catcher').css( 'background-color', '#1abc9c' );
    	catcherColor = 2;
    }else if(mouse.y>=(cTop+397) && mouse.y<(cTop+563)){
    	//console.log("yellow!");
    	$('#catcher').css( 'background-color', '#f39c12' );
    	catcherColor = 1;
    } else{
    	console.log("off!");
    }

    //console.log("cursor is at: " + mouse.x + ',' + mouse.y)
}

 var catcher = document.getElementById("catcher");



var totalScore=0;
$('.score').html(totalScore + ' Score');

//countdown timer vars
var currtime = 0;
var newTime = 0;
var min = 0;
var sec = 0;
var timeStr = '';
var watchID; //looping timer variable

function clockTime(pTime){
	// convert seconds into clock time format.
	min =  Math.floor(pTime / 60);
	sec = pTime % 60;
	if(sec >= 10){
		timeStr = '0' + min + ':' + sec;
	}else{
		timeStr = '0' + min + ':0' + sec;
	}
	
	return timeStr;
}

function updateWatch(){
	// scall this function every second
	currtime += 1;
	newTime = 120 - currtime;
	if(newTime > 0){
		$('.countdown').html(clockTime(newTime));
		drawTime(clockTime(newTime));
	} else {
		//Game over
		$('.countdown').html('00:00');
		drawTime('00:00');
		stopCountDownTimer();
		gameWin();

	}
	

}

function startCountDownTimer() {
	$('.countdown').html('02:00');
	drawTime('02:00');
	totalScore = 0;
	$('.score').html(totalScore + ' Score');
	drawScore(totalScore);
	//start da timer
	watchID = setInterval(updateWatch,1000);

}

function stopCountDownTimer(){
	clearInterval(watchID);
	currtime = 0;
}

function startGame(){
	clearCanvas();
    gamePlayBg();
	startCountDownTimer();
	startBlocks();
	animLoop();
}

function restartGame(){
	totalScore = 0;
	blocks = [];
	misses = 0; 

	clearCanvas();
    gamePlayBg();
	startCountDownTimer();
	startBlocks();
	animLoop();
}

var blocks = [];
var generateInterval;
var fallInterval;
function startBlocks(){
	generateInterval = setInterval(generateBlock,1000); //looping timer variable
	fallInterval = setInterval(fallBlock,4000); //looping timer variable
}
function stopBlocks(){
	clearInterval(generateInterval);
	clearInterval(fallInterval);
	for(var x=0;x<blocks.length;x++){
		if(blocks[x] != null){
			clearBlock(blocks[x]);
		}
	}
}

var blockCount = 0
function generateBlock(){
	blocks.push(new Block());

	drawBlock(blocks[blockCount]);
	blockCount++;
	//console.log("block generated");
}

var hit = document.getElementById("hit");
var miss = document.getElementById("miss");
var wrong = document.getElementById("wrong");

var misses = 0;
$('.misses').html(misses + ' Misses');
function fallBlock(){
	for(var x=0;x<120;x++){
		if(x < blocks.length && blocks[x] != null){
			if(blocks[x].h >0){
				clearBlock(blocks[x]);
				blocks[x].y += 2;
				drawBlock(blocks[x]);
				//console.log("Block"+ x + " Y position: " + +blocks[x].y);

				//less advanced hit box
				//blocks[x].x = left side of block
				//blocks[x].x + blocks[x].w = right side of block
				//mouse.x-cLeft-50 = left side of catcher
				//mouse.x-cLeft+50 = right side of catcher

				//(if the bottom of block is below the top of block and above 10pixels down of the catcher) and the ((left side of block is greater than left side of the catcher
				//and less than the right side) or right side of block is greater than left side of catcher and less than right side)

				if(((blocks[x].y + blocks[x].h) >= 670 && (blocks[x].y + blocks[x].h) <= 690) && (((blocks[x].x > mouse.x-cLeft-50) && (blocks[x].x < mouse.x-cLeft+50))
					|| ((blocks[x].x + blocks[x].w >= mouse.x-cLeft-50) && (blocks[x].x + blocks[x].w <= mouse.x-cLeft+50)))) {
					if(blocks[x].color == catcherColor){
						if(blocks[x].size==1){
							totalScore += 100;
						} else if(blocks[x].size==2){
							totalScore += 75;
						} else if(blocks[x].size==3){
							totalScore += 50;
						}
						hit.play();
						$('.score').html(totalScore + ' Score');
						drawScore(totalScore);
						clearBlock(blocks[x]);
						blocks[x] = destroyBlock();
					} else if(blocks[x].color != catcherColor){
						wrong.play();
						stopCountDownTimer();
						console.log("game lost");
						gameLose();
					}
				}
				//check if a block reaches the bottom
				if(blocks[x].y > 720){
					miss.play();
					clearBlock(blocks[x]);
					blocks[x] = destroyBlock();
				 	misses++;
				 	$('.misses').html(misses + ' Misses');
				 	drawMiss(misses + ' Misses');
				}
			}	
		}
	}

}


function destroyBlock(){
	this.c = 'red';
    this.x = 9999;
    this.y = 0;
    this.w = 0;
    this.h = 0;
}
function drawCatcher(){
	this.w = 100;
	this.h = 100;
}
function Block(){
	this.size = rand(3);
	this.color = rand(3);
	this.y = 0;

	if(this.size==1){
			this.w=20;
			this.h=20;
			this.x=placeRect(this.w);
			if(this.color==1){
				this.c = 'f39c12'; //yellow
			} else if(this.color==2){
				this.c = '1abc9c'; //green
			} else if(this.color==3){
				this.c = '34495e'; //blue
			}
		} else if(this.size==2){
			this.w=50;
			this.h=50;
			this.x=placeRect(this.w);
			if(this.color==1){
				this.c = 'f39c12'; //yellow
			} else if(this.color==2){
				this.c = '1abc9c'; //green
			} else if(this.color==3){
				this.c = '34495e'; //blue
			}
		} else if(this.size==3){
			this.h=80;
			this.w=80;
			this.x=placeRect(this.w);
			if(this.color==1){
				this.c = 'f39c12'; //yellow
			} else if(this.color==2){
				this.c = '1abc9c'; //green
			} else if(this.color==3){
				this.c = '34495e'; //blue
			}
		}


}


function drawBlock(block){
	context.beginPath();
    context.fillStyle = "#"+ block.c;
    context.fillRect(block.x,block.y,block.w,block.h);
    //console.log("x:" + this.x + " y:" + this.y + " w:" + this.w + " h:" + this.h + " c:" + this.c);
}

function clearBlock(block){
	context.beginPath();
    context.clearRect(block.x,block.y,block.w,block.h);
    //console.log("x:" + this.x + " y:" + this.y + " w:" + this.w + " h:" + this.h + " c:" + this.c);
}

//returns a random integer between 1 and x
function rand(x){
	return Math.floor((Math.random()*x)+1); 
}

//random place on canvas
function placeRect(w){
	return Math.floor((Math.random()*(960-w))+1); 
}

//reset the canvas

function clearCanvas(){
	context.clearRect(0,0,canvas.width,canvas.height);
}

function draw(){    
    //put catcher on
    // catcher.draw;

    $('#gameHeader').css('z-index', 3020);
    $('#gameCanvas').css('z-index', 3000);
    $('#catcher').css('z-index', 3020);
    gamePlayBg();

    $(".botOrange").hide();
    gameObj.showPlayScreen();
    

    //update animate all objects on the canvas
    update();
}

//main game logic: movement, score updates, collision, etc
function update(){
    //checks for movement
    //console.log(mouse.x);
    
    var x = mouse.x - cLeft - 50;
    if(x < 0){
    	$('#catcher').css( {left: 0} );
    } else if(mouse.x>cLeft+910){
    	$('#catcher').css( {left: 860} );
    } else {
    	$('#catcher').css( {left: x} );
    }
    //console.log("x:" + mouse.x + " cLeft + 910:" + cLeft+910);
    
    // for(var i=0;i < blocks.length;i++){
    // 	//move block
    // 	var blk = blocks[i];
    //  	blk.y += blk.vy;
    // }
    // console.log(blk.vy);
    fallBlock();
    
   
}

function animLoop(){
    init = requestAnimFrame(animLoop);   
    draw();
    //console.log("game started");
}

function btnClick(event){
    //console.log("canvas clicked");
    var myX = event.pageX;
    var myY = event.pageY;
    if(myX >= startBtn.x && myX <= startBtn.x  + startBtn.w){
         
        if(myY >= startBtn.y && myY <= startBtn.y + startBtn.h){
            console.log("start button clicked");  
            //start animation loop
            animLoop();
            
            //remove start button
            startBtn = {};
        }
    }
    if(myX >= restartBtn.x && myX <= restartBtn.x  + restartBtn.w){
         
        if(myY >= restartBtn.y && myY <= restartBtn.y + restartBtn.h){
            console.log("restart button clicked");  
            ball.y = 20;
            ball.x = 20;
            ball.vx = 6;
            ball.vy = 10;
            
            //start animation loop
            animLoop();
            

        }
    }
}

function gameWin(){
	//stop animation
    cancelRequestAnimFrame(init);

    //stop blocks from falling
    stopBlocks();

    //clear the canvas
    clearCanvas();
    

    $('#gameHeader').css('z-index', 2);
    $('#gameCanvas').css('z-index', 1);
    $('#catcher').css('z-index', 3);


    //show the restart button
    gameObj.showWinScreen();
}

function gameLose(){
    //stop animation
    cancelRequestAnimFrame(init);

    //stop blocks from falling
    stopBlocks();

    //clear the canvas
    clearCanvas();
    
    $('#gameHeader').css('z-index', 2);
    $('#gameCanvas').css('z-index', 1);
    $('#catcher').css('z-index', 3);

    //show the restart button
    gameObj.showLoseScreen();
}


// gameScreen.width = W;
// gameScreen.height = H;

// var gameObj = {
// 	// Start initializing objects, preloading assets and display start screen
// 	init: function() {
// 		console.log("Game initaized");
// 		// hide all game layers
// 		$(".gameLayer").hide();
// 	},
// 	showHomeScreen: function () {
// 		// hide all game layers 
// 		$(".gameLayer").hide();
// 		// show how screen
// 		$("#gameHomeScreen").show();
// 	},
// 	showWinScreen: function () {
// 		// hide all layers
// 		$(".gameLayer").hide();
// 		// show how screen
// 		$("#gameWinScreen").show();
		
// 	},
// 	showPlayScreen: function () {
// 		// hide all layers
// 		$(".gameLayer").hide();
// 		// show play screen layer
// 		$("#gameScreen").show();
// 	},
// 	showLoseScreen: function () {
// 		// hide all layers
// 		$(".gameLayer").hide();
// 		// show lose screen layer
// 		$("#gameLoseScreen").show();
// 	}
// }

// $(window).load(
// 	function() {
// 		gameObj.init();
// 	}
// );

var gameObj = {
	// Start initializing objects, preloading assets and display start screen
	init: function() {
		console.log("Game initaized");
		// hide all game layers
		$(".gameLayer").hide();
		// show home screen
		// $("#gameHomeScreen").show();
	},
	showHomeScreen: function () {
		// hide all game layers 
		$(".gameLayer").hide();
	},
	showLoseScreen: function () {
		// hide all layers
		$(".gameLayer").hide();
		// show lose screen
		$("#gameLoseScreen").show();
		$(".botOrange").show();
		
	},
	showPlayScreen: function () {
		// hide all layers
		$(".gameLayer").hide();
		// show play screen layer
		$("#gameHomeScreen").hide();
		$("#gameScreen").show();
		// $('#gameCanvas').css('z-index', 3000);
	},
	showWinScreen: function () {
		// hide all layers
		$(".gameLayer").hide();
		// show play screen layer
		$("#gameWinScreen").show();
		$(".botOrange").show();
	}
}

$(window).load(
	function() {
		gameObj.init();
	}
);

